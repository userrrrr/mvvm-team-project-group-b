﻿using System;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Enumerations;
using UnityEngine;

namespace Assets.Scripts.ViewModel.Contexts
{
    public class UserRegistrationContext : Context
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public Func<bool> RegistrationCommand { get; set; }

        public UserRegistrationContext()
        {
            RegistrationCommand = () =>
            {
                if (ViewModel.IsValidName(Name) && ViewModel.IsValidPassword(Password) && ViewModel.IsValidEmail(Email))
                {
                    var user = new UserProfile {Name = Name, Mail = Email, Password = Password};
                    return AppModel.AuthorizeCommand.Execute(JsonUtility.ToJson(user.ToDto()));
                }


                ViewModel.ResponseResultContext.IsSuccess = false;
                ViewModel.ResponseResultContext.Type = CommandType.Auth;
                ViewModel.ResponseResultContext.ErrorMessage = "Data validation error";

                return false;
            };
        }
    }
}