﻿using System;
using Assets.Scripts.Model;
using UnityEngine;

namespace Assets.Scripts.ViewModel.Contexts
{
    public class UserDataContext : Context
    {
        private string _name;
        private double _version;
        private double _money;
        private double _food;
        private double _materials;
        private int _buildingLevel;

        public Func<bool> UpgradeCommand { get; set; }

        public UserDataContext()
        {
            UpgradeCommand = () => AppModel.UpgradeCommand.Execute(JsonUtility.ToJson(AppModel.CurrentLogInedUser.ToDto()));
        }

        public string Name
        {
            get { return _name ?? ""; }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public double Version
        {
            get { return _version; }

            set
            {
                _version = value;
                OnPropertyChanged("Version");
            }
        }

        public double Money
        {
            get { return _money; }
            set
            {
                _money = value;
                OnPropertyChanged("Money");
            }
        }

        public double Food
        {
            get { return _food; }
            set
            {
                _food = value;
                OnPropertyChanged("Food");
            }
        }

        public double Materials
        {
            get { return _materials; }
            set
            {
                _materials = value;
                OnPropertyChanged("Materials");
            }
        }

        public int BuildingLevel
        {
            get { return _buildingLevel; }
            set
            {
                _buildingLevel = value;
                OnPropertyChanged("BuildingLevel");
            }
        }
    }
}