﻿using System;
using Assets.Scripts.Model;
using UnityEngine;

namespace Assets.Scripts.ViewModel.Contexts
{
    public class UserLoginContext : Context
    {
        private string _password;
        private string _email;

        public Func<bool> AuthCommand { get; set; }

        public UserLoginContext()
        {
            AuthCommand = () =>
            {
                var user = new UserProfile { Mail = Email, Password = Password };
                return AppModel.AuthorizeCommand.Execute(JsonUtility.ToJson(user.ToDto()));
            };
        }

        public string Password
        {
            get { return _password; }

            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }

        public string Email
        {
            get { return _email; }

            set
            {
                _email = value;
                OnPropertyChanged("Email");
            }
        }
    }
}