﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Assets.Scripts.Model;
using Assets.Scripts.Model.Enumerations;
using Assets.Scripts.ViewModel.Contexts;
using UnityEngine;

namespace Assets.Scripts.ViewModel
{
    public static class ViewModel
    {
        private const string NameValidationRegex = @"^[a-zA-Z0-9\_]+$";
        private const string EmailValidationRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        private const string PasswordValidationRegex = @"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$";

        public static UserLoginContext UserLoginContext;
        public static UserRegistrationContext RegistrationContext;
        public static UserDataContext UserDataContext;
        public static ResponseResultContext ResponseResultContext;

        static ViewModel()
        {
            UserLoginContext = new UserLoginContext();

            AppModel.CurrentLogInedUser.PropertyChanged += LoginDataChanged;

            UserDataContext = new UserDataContext();
            AppModel.CurrentUserData.PropertyChanged += UserDataChanged;

            RegistrationContext = new UserRegistrationContext();

            ResponseResultContext = new ResponseResultContext();
            AppModel.ResponseResult.PropertyChanged += ResponseResultChanged;
        }

        public static void Register()
        {
            if (IsValidName(RegistrationContext.Name) && IsValidEmail(RegistrationContext.Email) && IsValidPassword(RegistrationContext.Password))
            {
                var model = new UserProfile { Name = RegistrationContext.Name, Mail = RegistrationContext.Email, Password = RegistrationContext.Password };

                RegistrationContext.RegistrationCommand.Invoke();
            }
            else
            {
                ResponseResultContext.IsSuccess = false;
                ResponseResultContext.ErrorMessage = "Data validation error.";
                ResponseResultContext.Type = CommandType.Auth;
            }
        }

        public static bool LogIn()
        {
            if (IsValidEmail(UserLoginContext.Email) && IsValidPassword(UserLoginContext.Password))
            {
                return UserLoginContext.AuthCommand.Invoke();
            }

            throw new ArgumentException("Data validation error.");
        }

        public static UserData UpgradeBase()
        {
            return AppModel.UpgradeBase();
        }

        public static UserData UpdateUserResources()
        {
            return AppModel.RecalculateResources();
        }

        public static bool IsValidName(string name)
        {
            return Regex.IsMatch(name, NameValidationRegex) && name[0] != ' ';
        }

        public static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, EmailValidationRegex);
        }

        public static bool IsValidPassword(string password)
        {
            return Regex.IsMatch(password, PasswordValidationRegex);
        }

        private static void LoginDataChanged(object sender, PropertyChangedEventArgs args)
        {
            var user = (UserProfile)sender;
            var userField = user.GetType().GetProperty(args.PropertyName).GetGetMethod().Invoke(user, new object[] { });
            var contextField = UserLoginContext.GetType().GetProperty(args.PropertyName);

            if (contextField != null)
            {
                contextField.GetSetMethod().Invoke(UserLoginContext, new[] { userField });
            }
        }

        private static void UserDataChanged(object sender, PropertyChangedEventArgs args)
        {
            var user = (UserData)sender;
            var userField = user.GetType().GetProperty(args.PropertyName).GetGetMethod().Invoke(user, new object[] { });
            var contextField = UserDataContext.GetType().GetProperty(args.PropertyName);

            if (contextField != null)
            {
                contextField.GetSetMethod().Invoke(UserDataContext, new[] { userField });
            }
        }

        private static void ResponseResultChanged(object sender, PropertyChangedEventArgs args)
        {
            var resp = (ResponseResult)sender;
            var respField = resp.GetType().GetProperty(args.PropertyName).GetGetMethod().Invoke(resp, new object[] { });
            var contextField = ResponseResultContext.GetType().GetProperty(args.PropertyName);

            if (contextField != null)
            {
                contextField.GetSetMethod().Invoke(ResponseResultContext, new[] { respField });
            }
        }
    }
}
