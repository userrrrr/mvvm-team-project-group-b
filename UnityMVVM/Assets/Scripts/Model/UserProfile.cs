﻿using System.ComponentModel;
using Assets.Scripts.Model.DTO;
using JetBrains.Annotations;

namespace Assets.Scripts.Model
{
    public class UserProfile : INotifyPropertyChanged
    {
        private string _password;
        private string _uid;
        private string _mail;
        private string _name;

        private UserData _userData;

        public string UID
        {
            get { return _uid; }
            set
            {
                _uid = value;
                OnPropertyChanged("UID");
            }
        }

        public string Mail
        {
            get { return _mail; }
            set
            {
                _mail = value;
                OnPropertyChanged("Mail");
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public UserData UserData
        {
            get { return _userData; }
            set
            {
                _userData = value;
                OnPropertyChanged("UserData");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public UserProfileDto ToDto()
        {
            return new UserProfileDto{ Email = Mail, Name = Name, Password = Password, Uid = UID ?? ""};
        }
    }
}
