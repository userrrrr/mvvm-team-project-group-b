﻿using System.Text;
using Assets.Scripts.Model.Abstract;
using Assets.Scripts.Model.DTO;
using Assets.Scripts.Model.Enumerations;
using UnityEngine;

namespace Assets.Scripts.Model.Concrete
{
    public class AuthorizeCommand : Command
    {
        public AuthorizeCommand(string uri) : base(uri)
        {
        }

        public override bool Execute(string json)
        {
            TryConnect();

            var requestBuffer = Encoding.UTF8.GetBytes("auth\n" + json);

            using (var stream = _request.GetRequestStream())
            {
                stream.Write(requestBuffer, 0, requestBuffer.Length);
            }

            var response = _request.GetResponse();
            var responseBuffer = new byte[response.ContentLength];

            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    stream.Read(responseBuffer, 0, responseBuffer.Length);
                }
            }

            var userJson = Encoding.UTF8.GetString(responseBuffer);

            var profile = JsonUtility.FromJson<UserProfileDto>(userJson);

            if (profile == null)
            {
                AppModel.ResponseResult.ErrorMessage = "Authorization failed, try again.";
                AppModel.ResponseResult.IsSuccess = false;
                AppModel.ResponseResult.Type = CommandType.Auth;

                return false;
            }

            AppModel.CurrentLogInedUser.Mail = profile.Email;
            AppModel.CurrentLogInedUser.Name = profile.Name;
            AppModel.CurrentLogInedUser.UID = profile.Uid;
            AppModel.CurrentLogInedUser.Password = profile.Password;

            AppModel.ResponseResult.IsSuccess = true;

            return true;
        }
    }
}