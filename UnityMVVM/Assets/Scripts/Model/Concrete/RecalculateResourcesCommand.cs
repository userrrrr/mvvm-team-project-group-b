﻿using System.Text;
using Assets.Scripts.Model.Abstract;
using Assets.Scripts.Model.DTO;
using UnityEngine;

namespace Assets.Scripts.Model.Concrete
{
    public class RecalculateResourcesCommand : Command
    {
        public RecalculateResourcesCommand(string uri) : base(uri)
        {
        }

        public override bool Execute(string json)
        {
            TryConnect();

            var requestBuffer = Encoding.UTF8.GetBytes("recalc\n" + json);

            using (var stream = _request.GetRequestStream())
            {
                stream.Write(requestBuffer, 0, requestBuffer.Length);
            }

            var response = _request.GetResponse();
            var responseBuffer = new byte[response.ContentLength];

            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    stream.Read(responseBuffer, 0, responseBuffer.Length);
                }
            }

            var userDataJson = Encoding.UTF8.GetString(responseBuffer);
            var userData = JsonUtility.FromJson<UserDataDto>(userDataJson);

            AppModel.CurrentUserData.BuildingLevel = userData.BuildingLevel;
            AppModel.CurrentUserData.Food = userData.Food;
            AppModel.CurrentUserData.Materials = userData.Materials;
            AppModel.CurrentUserData.Money = userData.Money;
            AppModel.CurrentUserData.Version = userData.Version;
            AppModel.CurrentUserData.Name = AppModel.CurrentLogInedUser.Name;

            return true;
        }
    }
}