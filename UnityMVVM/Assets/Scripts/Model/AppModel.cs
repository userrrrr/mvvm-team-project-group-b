﻿using System;
using Assets.Scripts.Model.Concrete;
using UnityEngine;

namespace Assets.Scripts.Model
{
    public static class AppModel
    {
        public static string Uri = "http://localhost:8080/";

        public static AuthorizeCommand AuthorizeCommand;
        public static UpgradeCommand UpgradeCommand;
        public static RecalculateResourcesCommand RecalculateResourcesCommand;

        public static UserProfile CurrentLogInedUser { get; set; }
        public static UserData CurrentUserData { get; set; }
        public static ResponseResult ResponseResult { get; set; }

        static AppModel()
        {
            CurrentLogInedUser = new UserProfile();
            CurrentUserData = new UserData();
            ResponseResult = new ResponseResult();

            AuthorizeCommand = new AuthorizeCommand(Uri);    
            UpgradeCommand = new UpgradeCommand(Uri);
            RecalculateResourcesCommand = new RecalculateResourcesCommand(Uri);
        }

        public static UserData UpgradeBase()
        {
            UpgradeCommand.Execute(JsonUtility.ToJson(CurrentLogInedUser.ToDto()));

            return CurrentUserData;
        }

        public static UserData RecalculateResources()
        {
            RecalculateResourcesCommand.Execute(JsonUtility.ToJson(CurrentLogInedUser.ToDto()));

            return CurrentUserData;
        }
    }
}
