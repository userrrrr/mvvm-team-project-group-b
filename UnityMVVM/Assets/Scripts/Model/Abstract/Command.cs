﻿using System.Net;

namespace Assets.Scripts.Model.Abstract
{
    public abstract class Command
    {
        protected HttpWebRequest _request;
        public readonly string Uri;

        protected Command(string uri)
        {
            Uri = uri;
        }

        public void TryConnect()
        {
            try
            {
                _request = WebRequest.Create(Uri) as HttpWebRequest;

                if (_request != null)
                {
                    _request.Method = "POST";
                    _request.KeepAlive = true;
                }
            }
            catch
            {

            }
        }

        public abstract bool Execute(string json);
    }
}