﻿using System;
using System.Net;
using System.Text;

namespace Assets.Scripts.Model
{
    public class HttpClient
    {
        private HttpWebRequest _request;
        private readonly string Uri;

        public HttpClient(string uri)
        {
            Uri = uri;
        }

        public void TryConnect()
        {
            try
            {
                _request = WebRequest.Create(Uri) as HttpWebRequest;

                if (_request != null)
                {
                    _request.Method = "POST";
                    _request.KeepAlive = true;
                }
            }
            catch (Exception)
            {
               
            }
        }

        public string SendMessage(string json)
        {
            TryConnect();
            var requestBuffer = Encoding.UTF8.GetBytes(json);

            using (var stream = _request.GetRequestStream())
            {
                stream.Write(requestBuffer, 0, requestBuffer.Length);
            }

            var response = _request.GetResponse();
            var responseBuffer = new byte[response.ContentLength];

            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    stream.Read(responseBuffer, 0, responseBuffer.Length);
                }
            }

            return Encoding.UTF8.GetString(responseBuffer);
        }
    }
}
