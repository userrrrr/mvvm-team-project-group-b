﻿namespace Assets.Scripts.Model.Enumerations
{
    public enum CommandType
    {
        Auth,
        Upgrade,
        Recalc
    }
}