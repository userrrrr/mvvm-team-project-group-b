﻿using System.ComponentModel;
using Assets.Scripts.Model.Enumerations;
using JetBrains.Annotations;

namespace Assets.Scripts.Model
{
    public class ResponseResult : INotifyPropertyChanged
    {
        private bool _isSuccess;

        private string _errorMessage;

        public CommandType Type { get; set; }

        public bool IsSuccess
        {
            get { return _isSuccess; }
            set
            {
                _isSuccess = value;
                OnPropertyChanged("IsSuccess");
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                _errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}