﻿namespace Assets.Scripts.Model.DTO
{
    public class UserDataDto
    {
        public string Uid;
        public double Version;
        public double Money;
        public double Food;
        public double Materials;
        public int BuildingLevel; 
    }
}