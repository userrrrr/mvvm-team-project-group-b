﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ButtonClickBinder : MonoBehaviour
    {
        public string ContextName;
        public string CommandName;

        void Start()
        {
            var bindingObject = FindObjectOfType<UiHandler>();
            var bindings = bindingObject.GetBindingComponent();

            var command = bindings.GetCommand(ContextName, CommandName);

            GetComponent<Button>().onClick.AddListener(() => command.Invoke());
        }
    }
}