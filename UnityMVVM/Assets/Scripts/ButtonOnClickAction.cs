﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class ButtonOnClickAction : MonoBehaviour
    {
         public Action OnButtonClick { get; set; }
    }
}