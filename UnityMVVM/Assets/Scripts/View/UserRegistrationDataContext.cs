﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ViewModel.Contexts;

namespace Assets.Scripts.View
{
    public class UserRegistrationDataContext
    {
        public UserRegistrationContext RegistrationContext { get; set; }

        public UserRegistrationDataContext()
        {
            RegistrationContext = ViewModel.ViewModel.RegistrationContext;
        }
    }
}
