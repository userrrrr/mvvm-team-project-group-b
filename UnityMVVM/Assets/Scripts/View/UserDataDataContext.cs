﻿using System.ComponentModel;
using Assets.Scripts.ViewModel.Contexts;
using JetBrains.Annotations;

namespace Assets.Scripts.View
{
    public class UserDataDataContext
    {
        public UserDataContext UserDataContext { get; set; }

        public UserDataDataContext()
        {
            UserDataContext = ViewModel.ViewModel.UserDataContext;
        }
    }
}