﻿using System.ComponentModel;
using Assets.Scripts.ViewModel.Contexts;

namespace Assets.Scripts.View
{
    public class UserLoginDataContext
    {
        public UserLoginContext UserLoginContext { get; set; }

        public UserLoginDataContext()
        {
            UserLoginContext = ViewModel.ViewModel.UserLoginContext;
        }
    }
}
