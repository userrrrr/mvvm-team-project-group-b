﻿using Assets.Scripts.Model;

namespace Assets.Scripts.View
{
    public static class View
    {
        public static void TryRegisterUserProfile()
        {
            ViewModel.ViewModel.Register();
        }

        public static UserData TryUpgrade()
        {
            return ViewModel.ViewModel.UpgradeBase();
        }

        public static UserData UpdateUserData()
        {
            return ViewModel.ViewModel.UpdateUserResources();
        }

        public static bool TryLogIn()
        {
            return ViewModel.ViewModel.LogIn();
        }
    }
}
