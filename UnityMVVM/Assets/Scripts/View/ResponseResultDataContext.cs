﻿using System.ComponentModel;
using Assets.Scripts.ViewModel.Contexts;
using JetBrains.Annotations;

namespace Assets.Scripts.View
{
    public class ResponseResultDataContext : INotifyPropertyChanged
    {
        public ResponseResultContext ResponseResultContext { get; set; }

        public ResponseResultDataContext()
        {
            ResponseResultContext = ViewModel.ViewModel.ResponseResultContext;
            ResponseResultContext.PropertyChanged += OnContextPropertyChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnContextPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            OnPropertyChanged("ResponseResultContext");
        }
    }
}
