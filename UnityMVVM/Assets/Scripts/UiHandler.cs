﻿using System;
using System.Collections;
using Assets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UiHandler : MonoBehaviour
    {
        public GameObject LogInWindow;
        public GameObject RegistrationWindow;
        public GameObject GamePlayWindow;
        public GameObject ErrorWindow;
        public GameObject UpgradeErrorWindow;

        public Text ErrorText;

        public Binding Binding; 

        void Start()
        {
            if (Binding == null)
            {
                InitializeBindings();
            }

            LogInWindow.SetActive(true);
            RegistrationWindow.SetActive(false);
            GamePlayWindow.SetActive(false);
            ErrorWindow.SetActive(false);
        }

        public Binding GetBindingComponent()
        {
            if (Binding == null)
            {
                InitializeBindings();
            }

            return Binding;
        }

        public void ShowRegistrationWindow()
        {
            LogInWindow.SetActive(false);
            RegistrationWindow.SetActive(true);
            ErrorWindow.SetActive(false);
            GamePlayWindow.SetActive(false); 
        }

        public void ShowLogInWindow()
        {
            LogInWindow.SetActive(true);
            RegistrationWindow.SetActive(false);
            ErrorWindow.SetActive(false);
            GamePlayWindow.SetActive(false);
        }

        public IEnumerator UpdateResourcesInfo()
        {
            while (true)
            {
                View.View.UpdateUserData();

                yield return new WaitForSeconds(.1f);   
            }
        }

        public void HideErrorUpgradeWindow()
        {
            UpgradeErrorWindow.SetActive(false);
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        private void InitializeBindings()
        {
            gameObject.AddComponent<Binding>();
            Binding = GetComponent<Binding>();
            Binding.BindToErrorContext(ErrorWindow);

            Binding.AuthorizationSuccess += (sender, args) =>
            {
                GamePlayWindow.SetActive(true);
                StartCoroutine("UpdateResourcesInfo");
            };
        }
    }
}
