﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;

public class ExitGameClickHandler : MonoBehaviour, IPointerClickHandler
{
    public UiHandler UiHandler;

    public void OnPointerClick(PointerEventData eventData)
    {
        UiHandler.ExitGame();
    }
}
