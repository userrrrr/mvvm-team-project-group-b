﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class TextBinder : MonoBehaviour
    {
        public string FieldName;
        public string ContextName;

        public void Start()
        {
            var bindingObject = FindObjectOfType<UiHandler>();
            var bindings = bindingObject.GetBindingComponent();

            bindings.SubscribeOnTextChangedEvent(ContextName, FieldName, GetComponent<Text>());
        }
    }
}