﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Assets.Scripts.Model.Abstract;
using Assets.Scripts.Model.Enumerations;
using Assets.Scripts.View;
using Assets.Scripts.ViewModel.Contexts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Binding : MonoBehaviour
    {
        public UserLoginDataContext LoginDataContext { get; set; }
        public UserDataDataContext UserDataDataContext { get; set; }
        public UserRegistrationDataContext RegistrationDataContext { get; set; }
        public ResponseResultDataContext ResponseResultDataContext { get; set; }

        public EventHandler AuthorizationSuccess;
        public EventHandler UpgradeSuccess;

        void Awake()
        {
            Initialize();
        }

        void Initialize()
        {
            LoginDataContext = new UserLoginDataContext();
            UserDataDataContext = new UserDataDataContext();
            RegistrationDataContext = new UserRegistrationDataContext();
            ResponseResultDataContext = new ResponseResultDataContext();
        }

        public void SetInputFieldBinding(string contextName, string fieldName, object target)
        {
            var dataContext = GetType().GetProperties().First(p => p.Name == contextName);
            var dataContextInst = dataContext.GetGetMethod().Invoke(this, new object[] {});

            var context = dataContext.PropertyType.GetProperties().First(p => p.PropertyType.BaseType == typeof(Context));
            var contextInst = context.GetGetMethod().Invoke(dataContextInst, new object[] {});

            var property = context.PropertyType.GetProperty(fieldName);
            property.GetSetMethod().Invoke(contextInst, new object[] { ((InputField)target).text });
        }

        private string GetTextBinding(string contextName, string fieldName)
        {
            var dataContext = GetType().GetProperties().First(p => p.Name == contextName);
            var dataContextInst = dataContext.GetGetMethod().Invoke(this, new object[] {});

            var context = dataContext.PropertyType.GetProperties().First(p => p.PropertyType.BaseType == typeof(Context));
            var contextInst = context.GetGetMethod().Invoke(dataContextInst, new object[] {});

            var property = context.PropertyType.GetProperty(fieldName);

            return property.GetGetMethod().Invoke(contextInst, new object[] {}).ToString();
        }

        public void SubscribeOnTextChangedEvent(string contextName, string fieldName, Text uiText)
        {
            var dataContext = GetType().GetProperties().First(p => p.Name == contextName);
            var dataContextInst = dataContext.GetGetMethod().Invoke(this, new object[] { });

            var context = dataContext.PropertyType.GetProperties().First(p => p.PropertyType.BaseType == typeof(Context));
            var contextInst = context.GetGetMethod().Invoke(dataContextInst, new object[] { });

            var contextEvent = context.PropertyType.GetEvent("PropertyChanged");

            var func = new Func<string, string, string>((s, s1) =>
            {
                var res = GetTextBinding(s, s1);
                uiText.text = res.ToString();
                return res;
            });

            contextEvent.AddEventHandler(contextInst, new PropertyChangedEventHandler((sender, args) => func.Invoke(contextName, fieldName)));
        }

        public Func<bool> GetCommand(string contextName, string commandName)
        {
            var dataContext = GetType().GetProperties().First(p => p.Name == contextName);
            var dataContextInst = dataContext.GetGetMethod().Invoke(this, new object[] { });

            var context = dataContext.PropertyType.GetProperties().First(p => p.PropertyType.BaseType == typeof(Context));
            var contextInst = context.GetGetMethod().Invoke(dataContextInst, new object[] { });
            var property = context.PropertyType.GetProperty(commandName);

            return  (Func<bool>)property.GetGetMethod().Invoke(contextInst, new object[] {});
        }

        public void BindToErrorContext(GameObject errorWindow)
        {
            errorWindow.GetComponentInChildren<ButtonOnClickAction>().OnButtonClick = () => errorWindow.SetActive(false);
            var action = errorWindow.GetComponentInChildren<ButtonOnClickAction>().OnButtonClick;

            errorWindow.GetComponentInChildren<Button>().onClick.AddListener(action.Invoke);

            ResponseResultDataContext.PropertyChanged += (sender, args) =>
            {
                var context = (ResponseResultDataContext) sender;

                if (!(context.ResponseResultContext.IsSuccess))
                {
                    errorWindow.SetActive(true);
                    action = () => errorWindow.SetActive(false);
                    return;
                }

                if (context.ResponseResultContext.Type == CommandType.Auth)
                    AuthorizationSuccess(context, new EventArgs());

                if (context.ResponseResultContext.Type == CommandType.Upgrade)
                    UpgradeSuccess(context, new EventArgs());
            };
        }
    }
}
