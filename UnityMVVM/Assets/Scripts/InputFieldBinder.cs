﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ViewModel.Contexts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class InputFieldBinder : MonoBehaviour
    {
        public string FieldName;
        public string ContextName; 

        public void Start()
        {
            var bindingObject = FindObjectOfType<UiHandler>();
            var bindings = bindingObject.GetBindingComponent();

            GetComponent<InputField>().onEndEdit.AddListener(arg0 => bindings.SetInputFieldBinding(ContextName, FieldName, GetComponent<InputField>()));    
        }
    }
}