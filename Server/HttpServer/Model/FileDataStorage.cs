﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HttpServer.Model
{
    class FileDataStorage
    {
        private readonly string _filePath;

        public FileDataStorage(string filePath)
        {
            _filePath = filePath;
        }

        public bool SaveProfile(string profile)
        {
            try
            {
                using (var stream = new FileStream(_filePath, FileMode.Create))
                {
                    var bytes = System.Text.Encoding.Default.GetBytes(profile);

                    stream.Write(bytes, 0, profile.Length);

                    stream.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<string> ResolveProfiles()
        {
            var fileInfo = new FileInfo(_filePath);
            var profiles = new List<string>();

            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException();
            }

            using (var fileReader = fileInfo.OpenText())
            {
                while (!fileReader.EndOfStream)
                {
                    profiles.Add(fileReader.ReadLine());
                }
            }

            return profiles;
        }
    }
}