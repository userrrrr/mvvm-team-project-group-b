﻿using System;

namespace Assets.Scripts.Model
{
    public class UserData
    {
        public const int MinBuildingLevel = 1;
        public const int MaxBuildingLevel = 25;
        public const int BuildingUpgradeCostPerResource = 500;
        public const double VersionStep = 0.0001;

        private int _buildingLevel;

        public string UID { get; set; }

        public double Version { get; set; }

        public double Money { get; set; }

        public double Food { get; set; }

        public double Materials { get; set; }

        public int BuildingLevel
        {
            get { return _buildingLevel; }
            set
            {
                if (value < MinBuildingLevel)
                {
                    _buildingLevel = MinBuildingLevel;
                }
                else if (value > MaxBuildingLevel)
                {
                    _buildingLevel = MaxBuildingLevel;
                }
                else
                {
                    _buildingLevel = value;
                }
            }
        }

        public DateTime LastUpdateTime { get; set; }

        public UserData()
        {
            Version = VersionStep;
            BuildingLevel = 1;
        }

        public UserData RecalculateResources()
        {
            var hours = (DateTime.UtcNow - LastUpdateTime).Hours;

            Money += BuildingLevel * BuildingUpgradeCostPerResource * hours;
            Food += BuildingLevel * BuildingUpgradeCostPerResource * hours;
            Materials += BuildingLevel * BuildingUpgradeCostPerResource * hours;
            LastUpdateTime = DateTime.UtcNow;

            return this;
        }

        public bool CanIncreaseBuildinLevel()
        {
            var upgradeCost = GetUpgradeCost();

            return upgradeCost <= Money && upgradeCost <= Food && upgradeCost <= Materials;
        }

        public void IncreaseBuildingLevel()
        {
            if (!CanIncreaseBuildinLevel())
            {
                return;
            }

            var upgradeCost = GetUpgradeCost();

            Money -= upgradeCost;
            Food -= upgradeCost;
            Materials -= upgradeCost;
            BuildingLevel++;

            Version += VersionStep;
            LastUpdateTime = DateTime.UtcNow;
        }

        private int GetUpgradeCost()
        {
            return (BuildingLevel * BuildingUpgradeCostPerResource) + BuildingUpgradeCostPerResource;
        }

        public UserData Upgrade()
        {
            RecalculateResources();

            if (CanIncreaseBuildinLevel())
            {
                IncreaseBuildingLevel();
            }
            else
            {
                return null;
            }

            return this;
        }
    }
}