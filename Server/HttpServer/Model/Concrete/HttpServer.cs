﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Assets.Scripts.Model;
using Assets.Scripts.Model.DTO;
using HttpServer.Model.Abstract;
using Newtonsoft.Json;

namespace HttpServer.Model.Concrete
{
    public class HttpServer
    {
        public HttpListener Listener { get; set; }
        private FileDataStorage _dataStorage;
        private List<UserProfile> _userProfiles; 
        private IOutputManager _outputManager;

        private Dictionary<string, Func<string, string>> _commands; 

        public HttpServer(string prefix, IOutputManager outputManager, string filePath)
        {
            _outputManager = outputManager;
            _dataStorage = new FileDataStorage(filePath);
            _userProfiles = new List<UserProfile>();
            InitializeCommands();
            var profiles =_dataStorage.ResolveProfiles();

            foreach (var profile in profiles)
            {              
                _userProfiles.Add(JsonConvert.DeserializeObject<UserProfile>(profile));
            }

            Listener = new HttpListener();
            Listener.Prefixes.Add(prefix);

            AppDomain.CurrentDomain.ProcessExit += Stop;
        }

        public void Start()
        {
            Listener.Start();
            _outputManager.ShowMessage("Run server...");

            while (true)
            {           
                var context = Listener.GetContext();
                var request = context.Request;

                var bufferr = new Byte[request.ContentLength64];
                var recievedMessage = string.Empty;

                using (var data = context.Request.InputStream)
                {
                    data.Read(bufferr, 0, (int)request.ContentLength64);
                    recievedMessage = System.Text.Encoding.UTF8.GetString(bufferr);
                    _outputManager.ShowMessage(String.Format("The message  - {0} had received from client at {1} ", recievedMessage, DateTime.UtcNow));
                }


                var message = ProcessMessage(recievedMessage);
                var response = context.Response;

                _outputManager.ShowMessage(message);

                var buffer = System.Text.Encoding.UTF8.GetBytes(message);
                response.ContentLength64 = buffer.Length;

                var outputStream = response.OutputStream;
                outputStream.Write(buffer, 0, buffer.Length);
                outputStream.Close();

                _outputManager.ShowMessage("Continue listening...");
            }
        }

        private string ProcessMessage(string receivedMessage)
        {
            var data = receivedMessage.Split('\n');
            receivedMessage = receivedMessage.Replace(data[0], "");

            switch (data[0])
            {
                case "auth":
                    return _commands["auth"].Invoke(receivedMessage);
                case "upgrade":
                    return _commands["upgrade"].Invoke(receivedMessage);
                case "recalc":
                    return _commands["recalc"].Invoke(receivedMessage);
                default:
                    return "";
            }
        }

        private string AuthorizeUser(string userJson)
        {
            var profile = JsonConvert.DeserializeObject<UserProfileDto>(userJson);

            if (string.IsNullOrWhiteSpace(profile.Name) && string.IsNullOrWhiteSpace(profile.Uid))
            {
                var authorizedProfile = Identify(profile);

                if (authorizedProfile != null)
                {
                    return JsonConvert.SerializeObject(authorizedProfile.ToDto());
                }
                else
                {
                    return "";
                }
            }

            var newProfile = Create(profile);

            return newProfile == null ? "" : JsonConvert.SerializeObject(newProfile.ToDto());
        }

        private UserProfile Identify(UserProfileDto userProfile)
        {
            var user = _userProfiles.FirstOrDefault(u => u.Mail == userProfile.Email);

            if (user != null)
            {
                if (user.Password == userProfile.Password)
                {
                    return user;
                }
            }

            return null;
        }

        private UserProfile Create(UserProfileDto userProfileDto)
        {
            var user = _userProfiles.FirstOrDefault(u => u.Mail == userProfileDto.Email);
            UserProfile newUserProfile;

            if (user != null)
            {
                return null;
            }

            lock (_userProfiles)
            {
                newUserProfile = new UserProfile
                {
                    Mail = userProfileDto.Email,
                    Name = userProfileDto.Name,
                    Password = userProfileDto.Password,
                    Uid = Guid.NewGuid().ToString()
                };

                newUserProfile.UserData = new UserData {UID = newUserProfile.Uid};

                _userProfiles.Add(newUserProfile);
            }

            return newUserProfile;
        }

        public void Stop(object sender, EventArgs eventArgs)
        {
            Listener.Stop();

            foreach (var userProfile in _userProfiles)
            {
                _dataStorage.SaveProfile(JsonConvert.SerializeObject(userProfile));
            }
        }

        public string UpgradeUserBase(string userProfile)
        {
            var userId = (JsonConvert.DeserializeObject<UserProfile>(userProfile)).Uid;
            var existedProfile = _userProfiles.FirstOrDefault(u => u.Uid == userId);

            if (existedProfile != null)
            {
                var userData = existedProfile.UserData;
                var upgradedData = userData.Upgrade();

                if (upgradedData == null)
                {
                    return "";
                }

                return JsonConvert.SerializeObject(upgradedData);
            }

            return "";
        }

        public string RecalcResources(string userProfile)
        {
            var userId = (JsonConvert.DeserializeObject<UserProfile>(userProfile)).Uid;
            var existedProfile = _userProfiles.FirstOrDefault(u => u.Uid == userId);

            if (existedProfile != null)
            {
                var userData = existedProfile.UserData;
                var upgradedData = userData.RecalculateResources();

                return JsonConvert.SerializeObject(upgradedData);
            }

            return "";
        }

        private void InitializeCommands()
        {
            _commands = new Dictionary<string, Func<string, string>>
            {
                {"auth", AuthorizeUser},
                {"upgrade", UpgradeUserBase },
                {"recalc", RecalcResources }
            };
        }
    }
}