﻿using System;
using HttpServer.Model.Abstract;

namespace HttpServer.Model.Concrete
{
    public class ConsoleOutputManager : IOutputManager
    {
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }

        public string GetMessageFromUi()
        {
            Console.WriteLine("Input your data: ");

            var inputData = string.Empty;

            while (true)
            {
                inputData = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(inputData))
                {
                    Console.WriteLine("Wrong data, try again.");
                    continue;
                }

                break;
            }

            return inputData;
        }
    }
}