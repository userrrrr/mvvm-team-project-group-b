﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using Assets.Scripts.Model.DTO;

public class UserProfile
{
    private string _uid;
    private string _mail;
    private string _name;
    private string _password;

    private UserData _userData;

    public string Uid
    {
        get { return _uid; }
        set { _uid = value; }
    }

    public string Mail
    {
        get { return _mail; }
        set { _mail = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string Password
    {
        get { return _password; }
        set { _password = value; }
    }

    public UserData UserData
    {
        get { return _userData; }
        set { _userData = value; }
    }

    public UserProfileDto ToDto()
    {
        return new UserProfileDto { Email = Mail, Name = Name, Password = Password, Uid = Uid ?? "" };
    }
}
