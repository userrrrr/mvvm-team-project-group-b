﻿namespace HttpServer.Model.Abstract
{
    public interface IOutputManager
    {
        void ShowMessage(string message);

        string GetMessageFromUi();
    }
}